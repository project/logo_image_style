LOGO IMAGE STYLE
----------------

INTRODUCTION
-----------
  Allows the use of image styles for the site logo.

INSTALLATION
-------------
  1. Install as you would normally install a contributed Drupal module. 
     See: https://www.drupal.org/node/895232 for further information.
  2. Go to Admin > Modules and enable the Logo image style module.

CONFIGURATION
-------------
  1. Configure settings at Admin > Appearance > Theme settings page.

REQUIREMENTS
------------
  The basic nextpre module has node dependencies, nothing special is required.
